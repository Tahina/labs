<?php 
$fonts = array(
	'Raleway' => 'http://fonts.googleapis.com/css?family=Raleway:400,300,200',
	'Open Sans' => 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800',
	'Open Sans Condensed' => 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700',
	'Oswald' => 'http://fonts.googleapis.com/css?family=Oswald:400,700',
	'PT Sans' => 'http://fonts.googleapis.com/css?family=PT+Sans:400,700',
	'Cabin' => 'http://fonts.googleapis.com/css?family=Cabin:400,500,600,700'
);

function print_font_style($selectedFonts = array()) {
	global $fonts;	
		
	foreach ($selectedFonts as $sfont) {
		if($sfont != 'Verdana') { echo "<link href='" . $fonts[$sfont] . "' rel='stylesheet' type='text/css'>"; }
	}	
}

function font_select($selectName = 'font-family', $currentItem = '') {
	global $fonts;	
	echo '<select name="' . $selectName . '">';
		echo '<option value="">Select</option>';
	foreach ($fonts as $key => $value) {
		$selected = ($currentItem == $key)? 'selected=selected': '';
		echo '<option ' . $selected . '>' . $key . '</option>';
	}
	echo '</select>';
}

function font_size_select($selectName = 'font-size', $currentItem = '') {
	
	$size = array();

	echo '<select name="' . $selectName . '">';
	foreach (range(7, 65) as $number) {
		$selected = ($currentItem == $number)? 'selected=selected': '';
		echo '<option ' . $selected . '>' . $number . '</option>';
	}
	echo '</select>';	
}

function font_weight_select($selectName = 'font-weight', $currentItem = '') {
	
	$size = array();

	echo '<select name="' . $selectName . '">';
	foreach (range(400, 900, 100) as $number) {
		$selected = ($currentItem == $number)? 'selected=selected': '';
		echo '<option ' . $selected . '>' . $number . '</option>';
	}
	echo '</select>';	
}

function text_transform_select($selectName = 'text-transform', $currentItem = '') {
	
	$size = array();

	echo '<select name="' . $selectName . '">';	
		echo '<option value="">None</option>';
		$selected = ($currentItem == 'capitalize')? 'selected=selected': '';
		echo '<option ' . $selected . ' value="capitalize">Capitalize</option>';
		$selected = ($currentItem == 'uppercase')? 'selected=selected': '';
		echo '<option ' . $selected . ' value="uppercase">Uppercase</option>';
		$selected = ($currentItem == 'lowercase')? 'selected=selected': '';
		echo '<option ' . $selected . ' value="lowercase">Lowercase</option>';
	echo '</select>';	
}
?>