<?php 
$root = 'http://labs.vanileo.com/font-tester/';
//$root = 'http://127.0.0.1/labs/font-tester/';

/* general */
$fontFamily = (isset($_GET['font-family']) && trim($_GET['font-family']) != '')? htmlentities($_GET['font-family']) : 'Verdana';
$fontSize = (isset($_GET['font-size']))? htmlentities($_GET['font-size']) : '12';

/* h1 */
$h1FontFamily = (isset($_GET['h1-font-family']) && trim($_GET['h1-font-family']) != '')? htmlentities($_GET['h1-font-family']) : $fontFamily;
$h1FontSize = (isset($_GET['h1-font-size']))? htmlentities($_GET['h1-font-size']) : '32';
$h1FontWeight = (isset($_GET['h1-font-weight']))? htmlentities($_GET['h1-font-weight']) : '700';
$h1TextTransform = (isset($_GET['h1-text-transform']))? htmlentities($_GET['h1-text-transform']) : 'none';
/* h2 */
$h2FontFamily = (isset($_GET['h2-font-family']) && trim($_GET['h2-font-family']) != '')? htmlentities($_GET['h2-font-family']) : $fontFamily;
$h2FontSize = (isset($_GET['h2-font-size']))? htmlentities($_GET['h2-font-size']) : '24';
$h2FontWeight = (isset($_GET['h2-font-weight']))? htmlentities($_GET['h2-font-weight']) : '700';
$h2TextTransform = (isset($_GET['h2-text-transform']))? htmlentities($_GET['h2-text-transform']) : 'none';
/* h3 */
$h3FontFamily = (isset($_GET['h3-font-family']) && trim($_GET['h3-font-family']) != '')? htmlentities($_GET['h3-font-family']) : $fontFamily;
$h3FontSize = (isset($_GET['h3-font-size']))? htmlentities($_GET['h3-font-size']) : '14';
$h3FontWeight = (isset($_GET['h3-font-weight']))? htmlentities($_GET['h3-font-weight']) : '700';
$h3TextTransform = (isset($_GET['h3-text-transform']))? htmlentities($_GET['h3-text-transform']) : 'none';
/* h4 */
$h4FontFamily = (isset($_GET['h4-font-family']) && trim($_GET['h4-font-family']) != '')? htmlentities($_GET['h4-font-family']) : $fontFamily;
$h4FontSize = (isset($_GET['h4-font-size']))? htmlentities($_GET['h4-font-size']) : '14';
$h4FontWeight = (isset($_GET['h4-font-weight']))? htmlentities($_GET['h4-font-weight']) : '700';
$h4TextTransform = (isset($_GET['h4-text-transform']))? htmlentities($_GET['h4-text-transform']) : 'none';

/* gathering all & checking doublons */
$allFonts = array();
if(!in_array($fontFamily, $allFonts)) $allFonts[] = $fontFamily;
if(!in_array($h1FontFamily, $allFonts)) $allFonts[] = $h1FontFamily;
if(!in_array($h2FontFamily, $allFonts)) $allFonts[] = $h2FontFamily;
if(!in_array($h3FontFamily, $allFonts)) $allFonts[] = $h3FontFamily;
if(!in_array($h4FontFamily, $allFonts)) $allFonts[] = $h4FontFamily;

require('google-fonts.php');
?>
<!DOCTYPE html>
<html lang='fr'>
<head>
	<title>Font tester</title>

	<?php print_font_style($allFonts) ?>
		
	<link href="style.css" rel="stylesheet" />

	<style type="text/css">
		body, p { 
			font-family: '<?php echo $fontFamily ?>'; 
			font-size: <?php echo $fontSize ?>px;			
			font-weight: normal;
		}
		h1 { 
			font-family: '<?php echo $h1FontFamily ?>';
			font-size: <?php echo $h1FontSize ?>px !important;
			font-weight: <?php echo $h1FontWeight ?>;
			text-transform: <?php echo $h1TextTransform ?>
		}
		h2 { 
			font-family: '<?php echo $h2FontFamily ?>';
			font-size: <?php echo $h2FontSize ?>px !important;
			font-weight: <?php echo $h2FontWeight ?>;
			text-transform: <?php echo $h2TextTransform ?>
		}
		h3 { 
			font-family: '<?php echo $h3FontFamily ?>';
			font-size: <?php echo $h3FontSize ?>px !important;
			font-weight: <?php echo $h3FontWeight ?>;
			text-transform: <?php echo $h3TextTransform ?>
		}
		h4 { 
			font-family: '<?php echo $h4FontFamily ?>';
			font-size: <?php echo $h4FontSize ?>px !important;
			font-weight: <?php echo $h4FontWeight ?>;
			text-transform: <?php echo $h4TextTransform ?>
		}
		
		.uppercase { text-transform: uppercase; }
		.italic { font-style: italic; }	
		.small { font-size: 10px; }
		.huge { font-size: 40px; }		

		td { padding: 3px 10px; font-family: 'Verdana' !important; font-weight: bold; }
	</style>	
</head>

<body>

	<div id="font-bar">
		<form method="get" action="<?php echo $root ?>">
			<table cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td valign="top">
						General<br>
						<?php font_select('font-family', $fontFamily); ?>
						<?php font_size_select('font-size', $fontSize) ?> px
					</td>
					<td>
						h1<br>
						<?php font_select('h1-font-family', $h1FontFamily); ?><br>
						<?php font_size_select('h1-font-size', $h1FontSize); ?> px<br>
						<?php font_weight_select('h1-font-weight', $h1FontWeight); ?><br>
						<?php text_transform_select('h1-text-transform', $h1TextTransform) ?>
					</td>
					<td>
						h2<br>
						<?php font_select('h2-font-family', $h2FontFamily); ?><br>
						<?php font_size_select('h2-font-size', $h2FontSize); ?> px<br>
						<?php font_weight_select('h2-font-weight', $h2FontWeight); ?><br>
						<?php text_transform_select('h2-text-transform', $h2TextTransform) ?>
					</td>
					<td>
						h3<br>
						<?php font_select('h3-font-family', $h3FontFamily); ?><br>
						<?php font_size_select('h3-font-size', $h3FontSize); ?> px<br>
						<?php font_weight_select('h3-font-weight', $h3FontWeight); ?><br>
						<?php text_transform_select('h3-text-transform', $h3TextTransform) ?>
					</td>
					<td>
						h4<br>
						<?php font_select('h4-font-family', $h4FontFamily); ?><br>
						<?php font_size_select('h4-font-size', $h4FontSize); ?> px<br>
						<?php font_weight_select('h4-font-weight', $h4FontWeight); ?><br>
						<?php text_transform_select('h4-text-transform', $h4TextTransform) ?>
					</td>
					<td>
						<input type="submit" value="Change" />
						<input onclick="document.location='<?php echo $root ?>'" type="button" value="Reset" />
					</td>
				</tr>
			</table>			
			
		</form>
	</div>
	
	<div style="width:700px;padding-left:20px;">

		<h1>This is a title in h1</h1>
		<p>Lorem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>

		<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </p>
		<h2>This is a title in h2</h2>
		<p>Lorem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>

		<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </p>
		<h3>This is a title in h3</h3>
		<p>Lorem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>

		<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </p>
		<h4>This is a title in h4</h4>
		<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. </p>

		<ul>
			<li>Lorem ipsum dolor sit amet</li>
			<li>Lorem ipsum dolor sit amet</li>
			<li>Lorem ipsum dolor sit amet</li>
			<li>Lorem ipsum dolor sit amet</li>
		</ul>

	</div>


</body>
</html>
