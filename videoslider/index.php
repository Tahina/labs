<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta http-equiv="Content-Type" />
    <title>SLIDER</title>
    <!-- jQuery library (served from Google) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- bxSlider Javascript file -->
	<script src="js/jquery.bxslider.min.js"></script>
	<!-- bxSlider CSS file -->
	<link href="css/jquery.bxslider.min.css" rel="stylesheet" />
</head>
<body>
	<ul class="bxslider">
	  <li><iframe width="560" height="315" src="https://www.youtube.com/embed/C4O6chwHLA4" frameborder="0" allowfullscreen></iframe></li>
	  <li><iframe width="560" height="315" src="https://www.youtube.com/embed/2Xk744838J4" frameborder="0" allowfullscreen></iframe></li>
	  <li><iframe width="560" height="315" src="https://www.youtube.com/embed/x7QGkDBXnLA" frameborder="0" allowfullscreen></iframe></li>
	  <li><iframe width="560" height="315" src="https://www.youtube.com/embed/0QZr0nu5bRs" frameborder="0" allowfullscreen></iframe></li>
	</ul>

	<script type="text/javascript">
		$(document).ready(function(){
		  $('.bxslider').bxSlider({
		  	minSlides: 3,
			maxSlides: 4,
			slideWidth: 170,
			slideMargin: 10
		  });
		});
	</script>
</body>
</html>    