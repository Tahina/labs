<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta http-equiv="Content-Type" />
    <title>SLIDER</title>
    <!-- jQuery library (served from Google) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- bxSlider Javascript file -->
	<script src="owl.carousel.min.js"></script>
	<!-- bxSlider CSS file -->
	<link href="assets/owl.carousel.css" rel="stylesheet" />
	<link href="assets/owl.theme.default.min.css" rel="stylesheet" />
</head>
<body>
	<div class="owl-carousel">
	    <div><img src="../images/1.jpg" alt="" /></div>
	    <div><img src="../images/2.jpg" alt="" /></div>
	    <div class="item-video"><a class="owl-video" href="https://www.youtube.com/watch?v=mMZZU5yJBGU"></a></div>
	    <div><img src="../images/4.jpg" alt="" /></div>
	    <div><img src="../images/5.jpg" alt="" /></div>
	    <div><img src="../images/6.jpg" alt="" /></div>
	    <div><img src="../images/7.jpg" alt="" /></div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
		  $('.owl-carousel').owlCarousel({
		  	items:1,
		  	merge:true,
	        loop:true,
	        margin:10,
	        video:true,
	        lazyLoad:true,
	        center:true,
	        responsive:{
	            480:{
	                items:2
	            },
	            600:{
	                items:4
	            }
	        }
		  });
		});
	</script>
</body>
</html>    