<?php
include('header.php');
// init
$errMsg = '';
$s = 0; // first sheet
$failed = 0;
$success = 0;
$doublon = 0;
$unfoundCirconscription = array();
$findme    = '/';

$circonscription = new circonscriptions();
$stats = new CirconscriptionStats();
$donors = new CirconscriptionDonors();
//$donor = new Donors();

$file = isset($_GET["file"]) ? $_GET["file"] : '';

// chk if xls file
if(!file_exists(IMPORT_DIR . $file)) {
    $errMsg = "Impossible de trouver le fichier $file";
}
// chk type
$tFile = explode('.', $file);
$ext = $tFile[sizeof($tFile) - 1];
if($ext != 'xls') {
    $errMsg = "Ce fichier n'est pas valide";
}

if(trim($file) != "" && trim($errMsg) == "") {
    require('reader.php');
    
    // treat
    $data = new Spreadsheet_Excel_Reader();
    $data->setOutputEncoding('CP1251');
    $data->read("docs/" . $file); ?>

    <table class="chart" border=1>
        <tr>
            <th class="col1">R&eacute;gion concern&eacute;e</th>
            <th class="col2">Nom de l'OSC</th>
            <th class="col3">Intitul&eacute;</th>
            <th class="col4">Montant du projet</th>
            <th class="col5">Coordonn&eacute;s de l'OSC</th>
        </tr>
        <?php
        for ($i = 2; $i <= $data->sheets[$s]['numRows']; $i++) {
            $cls = ($i%2 == 0)? '' : 'dark';
            //if($data->sheets[$s]['cells'][$i][1] != '') { ?>
                <tr class="<?php echo $cls ?>">
                    <td class="col1"><?php echo $data->sheets[$s]['cells'][$i][1]; ?></td>
                    <td class="col2"><?php echo $data->sheets[$s]['cells'][$i][2]; ?></td>
                    <td class="col3"><?php echo (trim($data->sheets[$s]['cells'][$i][3]) == '')? 'NA' : $data->sheets[$s]['cells'][$i][3]; ?></td>
                    <td class="col4">
                        <?php 
                        $fund = str_replace('.', ',', trim($data->sheets[$s]['cells'][$i][4]));
                        $arr = explode(',', $fund);                        
                        echo (sizeof($arr) > 1)? $fund : $fund . ',00'; 
                        ?>
                    </td>
                    <td class="col5"><?php echo $data->sheets[$s]['cells'][$i][5]; ?></td>
                </tr>                
            <?php 
            //}    
        } ?>
    </table>
    <?php
}
// delete file
//@unlink(IMPORT_DIR . $file);
include("header-html.php");
?>
<h1>Les donn&eacute;es import&eacute;es</h1>

<a href="index.php">Accueil</a> 
&raquo; <a href="<?php echo SITE_ROOT; ?>cp/circonscription-stats.php">Chiffres circonscription</a>
<br /><br />

<?php if(trim($errMsg) != "") { ?>
<div class="message"><?php echo $errMsg; ?></div>
<?php } ?>
<fieldset>
    Lignes ins&eacute;r&eacute;es : <strong><?php echo $success; ?></strong><br>
    Echecs : <strong><?php echo $failed; ?></strong><br />
    Doublons : <strong><?php echo $doublon; ?></strong><br />
    Circonscriptions non trouv&eacute;es : <?php echo implode(', ', $unfoundCirconscription); ?><br />
</fieldset>
<br>
<a href="<?php echo SITE_ROOT; ?>cp/import-circonscription-xls.php">Importer un nouveau fichier</a>

<?php include("footer.php"); ?>