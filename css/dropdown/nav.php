<nav>    <ul id="nav" class="dropdown dropdown-horizontal">
        <li class="current_page_item"><a href="http://vanileo.com/eiti" class="first_menu home" title="Retour &agrave; l'accueil">A</a></li>
        <li class="page_item page-item-166 page_item_has_children"><a href="http://vanileo.com/eiti/eiti/">EITI</a>
<ul class='children'>
	<li class="page_item page-item-184"><a href="http://vanileo.com/eiti/eiti/apercu/">Aperçu</a></li>
	<li class="page_item page-item-186"><a href="http://vanileo.com/eiti/eiti/mise-en-oeuvre/">Mise en oeuvre</a></li>
	<li class="page_item page-item-188"><a href="http://vanileo.com/eiti/eiti/statut/">Statut</a></li>
	<li class="page_item page-item-191"><a href="http://vanileo.com/eiti/eiti/regle-eiti/">Règle EITI</a></li>
	<li class="page_item page-item-193"><a href="http://vanileo.com/eiti/eiti/principes-et-criteres/">Principes et critères</a></li>
	<li class="page_item page-item-195 page_item_has_children"><a href="http://vanileo.com/eiti/eiti/organisations/">Organisations</a>
	<ul class='children'>
		<li class="page_item page-item-210"><a href="http://vanileo.com/eiti/eiti/organisations/organigramme/">Organigramme</a></li>
		<li class="page_item page-item-212 page_item_has_children"><a href="http://vanileo.com/eiti/eiti/organisations/equipe/">Equipe</a>
		<ul class='children'>
			<li class="page_item page-item-218"><a href="http://vanileo.com/eiti/eiti/organisations/equipe/champion/">Champion</a></li>
			<li class="page_item page-item-220"><a href="http://vanileo.com/eiti/eiti/organisations/equipe/secretariat-executif/">Secrétariat executif</a></li>
		</ul>
</li>
		<li class="page_item page-item-214"><a href="http://vanileo.com/eiti/eiti/organisations/comite-national/">Comité national</a></li>
	</ul>
</li>
	<li class="page_item page-item-197"><a href="http://vanileo.com/eiti/eiti/plan-de-travail/">Plan de travail</a></li>
</ul>
</li>
<li class="page_item page-item-168 page_item_has_children"><a href="http://vanileo.com/eiti/parties-prenantes/">Parties prenantes</a>
<ul class='children'>
	<li class="page_item page-item-178"><a href="http://vanileo.com/eiti/parties-prenantes/administration/">Administration</a></li>
	<li class="page_item page-item-180"><a href="http://vanileo.com/eiti/parties-prenantes/compagnies/">Compagnies</a></li>
	<li class="page_item page-item-182"><a href="http://vanileo.com/eiti/parties-prenantes/societe-civile/">Société civile</a></li>
</ul>
</li>
<li class="page_item page-item-130 page_item_has_children"><a href="http://vanileo.com/eiti/all-news/">News</a>
<ul class='children'>
	<li class="page_item page-item-38"><a href="http://vanileo.com/eiti/all-news/actualites/">Actualités</a></li>
</ul>
</li>
<li class="page_item page-item-40"><a href="http://vanileo.com/eiti/reunions/">Réunions</a></li>
<li class="page_item page-item-171 page_item_has_children"><a href="http://vanileo.com/eiti/publications/">Publications</a>
<ul class='children'>
	<li class="page_item page-item-61"><a href="http://vanileo.com/eiti/publications/communications/">Communications</a></li>
	<li class="page_item page-item-59"><a href="http://vanileo.com/eiti/publications/rapports/">Rapports</a></li>
	<li class="page_item page-item-66 page_item_has_children"><a href="http://vanileo.com/eiti/publications/legislations/">Législations</a>
	<ul class='children'>
		<li class="page_item page-item-68"><a href="http://vanileo.com/eiti/publications/legislations/mines/">Mines</a></li>
		<li class="page_item page-item-80"><a href="http://vanileo.com/eiti/publications/legislations/petroles/">Pétroles</a></li>
	</ul>
</li>
</ul>
</li>
<li class="page_item page-item-410"><a href="http://vanileo.com/eiti/formulaires/">Formulaires</a></li>
<li class="page_item page-item-174 page_item_has_children"><a href="http://vanileo.com/eiti/opportunites/">Opportunités</a>
<ul class='children'>
	<li class="page_item page-item-202"><a href="http://vanileo.com/eiti/opportunites/appel-a-manifestations/">Appel à manifestations</a></li>
</ul>
</li>
    </ul>
    </nav>
