/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Cat Arriola | http://astrodiva.journalspace.com/ */

function fixImgs(whichId, maxW) {
  var pix=document.getElementById(whichId).getElementsByTagName('img');
  for (i=0; i<pix.length; i++) {
    w=pix[i].width;
    h=pix[i].height;
    if (w > maxW) {
      f=1-((w - maxW) / w);
      pix[i].width=w * f;
      pix[i].height=h * f;
    }
  }
}

// Multiple onload function created by: Simon Willison
// http://simonwillison.net/2004/May/26/addLoadEvent/
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

addLoadEvent(function() {
  fixImgs('post', 440);  // ('element ID', maximum width)
});

