jQuery(window).on('resize', function($){
    place_slideshow();
});

jQuery(document).ready(
  function($) { 
    place_slideshow();
  }  
);

function place_slideshow(){
    var windowWidth = jQuery(window).width();
    var newHeight = get_slider_height(windowWidth);
    var frameHeight = 360;
    var minusTop = 0;

    if((windowWidth - frameHeight) > 0) {    	
	    var minusTop = (newHeight - frameHeight) / 2;
	    minusTop = Math.floor(minusTop) + 1;
	    minusTop = minusTop * -1;
	} else {
		jQuery('#header_banner').css('height', windowWidth + 'px');
	}
    //alert(minusTop);
    jQuery('#margin_top').text('ecr: ' + windowWidth + '/' + 'New img height :' + newHeight + '/' + minusTop + 'px');

    // apply height to div's css
    // $('#slideshow').css('position','absolute');
    // $('#slideshow').css('top','-50%');
    jQuery('#slideshow').css('margin-top', minusTop + 'px');
}

/*
Gets slider height depending on screen width
*/
function get_slider_height(screenWidth) {
	var imgWidth = 1024;
	var imgHeight = 682;
	//alert(screenWidth + ' * ' + imgHeight + '/' + imgWidth);
	var newHeight = (screenWidth * imgHeight) / imgWidth;
	newHeight = Math.floor(newHeight) + 1;
	return newHeight;
}