<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Heartfelt | A WordPress Theme for Charities and Nonprofits</title>
  <script type='text/javascript' src='http://localhost/IBC/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
  <script type='text/javascript' src='jquery.waypoints.min.js'></script>  
  <script type="text/javascript">
  	jQuery(function($){
		$(document).ready(function(){
			$('.rescue-progressbar').waypoint(function() {
				$('.rescue-progressbar').each(function(){
					$(this).find('.rescue-progressbar-bar').animate({ 
						width: $(this).attr('data-percent') }, 1500 );
				});
			}, { offset: 500 });
		});
	});
  </script>
  <style type="text/css">
  	/**
	 * 1. Correct `inline-block` display not defined in IE 8/9.
	 * 2. Normalize vertical alignment of `progress` in Chrome, Firefox, and Opera.
	 */
	audio,
	canvas,
	progress,
	video {
	  display: inline-block;
	  /* 1 */
	  vertical-align: baseline;
	  /* 2 */
	}
  	
  	.rescue-progressbar {
		margin-bottom: 0;
		height: 45px;
	}
	.rescue-progressbar .rescue-progressbar-bar, .rescue-progressbar .rescue-progressbar-title span {
		height: 45px;
	}
	.rescue-progressbar .rescue-progressbar-title span, .rescue-progressbar .rescue-progress-bar-percent {
		line-height: 45px;
	}

	/*----------------------------------------------------*/
	/*  Progress Bar
	/*----------------------------------------------------*/
	.rescue-progressbar { 
		position: relative; display: block; 
		margin-bottom: 15px; width: 100%; background: #eee; height: 45px; 
		border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; 
		-webkit-transition: 0.4s linear; 
		-moz-transition: 0.4s linear; 
		-ms-transition: 0.4s linear; 
		-o-transition: 0.4s linear; transition: 0.4s linear; 
		-webkit-transition-property: width, background-color; 
		-moz-transition-property: width, background-color; 
		-ms-transition-property: width, background-color; 
		-o-transition-property: width, background-color; 
		transition-property: width, background-color; 
	}
	.rescue-progressbar-title { position: absolute; top: 0; left: 0; font-weight: bold; font-size: 13px; color: #fff; background: #6adcfa; -webkit-border-top-left-radius: 3px; -webkit-border-bottom-left-radius: 4px; -moz-border-radius-topleft: 3px; -moz-border-radius-bottomleft: 3px; border-top-left-radius: 3px; border-bottom-left-radius: 3px; }
	.rescue-progressbar-title span { display: block; background: rgba(0,0,0,0.1); padding: 0 20px; height: 35px; line-height: 35px; -webkit-border-top-left-radius: 3px; -webkit-border-bottom-left-radius: 3px; -moz-border-radius-topleft: 3px; -moz-border-radius-bottomleft: 3px; border-top-left-radius: 3px; border-bottom-left-radius: 3px; }
	.rescue-progressbar-bar { height: 35px; width: 0px; background: #6adcfa; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; }
	.rescue-progress-bar-percent { position: absolute; right: 10px; top: 0; font-size: 11px; height: 45px; line-height: 35px; color: #400; color: rgba(0,0,0,0.4); }

  	</style>

  	<link rel="stylesheet" type="text/css" href="goalProgress.css" />
	<script type="text/javascript" src="goalProgress.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('#sample_goal').goalProgress({
				goalAmount: 600,
				currentAmount: 400,
				textBefore: '$',
				textAfter: ' raised.'
			});
		});
	</script>
</head>

<body>
	<div style="height:800px; background-color:#ccc;margin-bottom:30px;"></div>
	<div class="rescue-progressbar rescue-clearfix  rescue-all" data-percent="75%">
		<div class="rescue-progressbar-title" style="background: #fec840;">
			<span>Volunteers</span>
		</div>
		<div class="rescue-progressbar-bar" style="background: #fec840;"></div>
		<div class="rescue-progress-bar-percent">75%</div>
	</div>
	<hr>
	<div class="container">
		<div id="sample_goal"></div>
	</div>
	<div style="height:400px; background-color:#ccc;margin-bottom:30px;"></div>
</body>	
</html>