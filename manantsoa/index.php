<style type="text/css">
.caseFerie {
    padding-top: 2px;
    margin-bottom: 1px;
    margin-top: 1px;
    text-align: center;
    font-size: 9px;
    width: 25px;
    background: none repeat scroll 0 0 #808080;
    color: #ffffff;
    height: 12px;
    line-height: 100%;
    border-radius: 3px 3px 3px 3px;
}

.css_tableau {
    background-color: #ebeff2;
    border-color: #3DA9C9;
    border-width: 1px;
    border-style: solid;
    border-collapse: separate;
    border-spacing: 1px;
}

.css_tableau th {
    font-size: 12px;
    background-color: #3DA9C9;
    color: #FFFFFF;
    font-weight: bold;
    padding: 0px 0px 0px 0px;
    vertical-align: middle;
    text-align: center;
    border-collapse: separate;
}

.css_tableau tr {
    font-size: 12px;
    background-color: #FFFFFF;
    padding: 0px 0px 0px 0px;
    vertical-align: left;
    text-align: center;
    border-collapse: separate;
}

.css_tableau td div {
    font-size: 12px;
    background-color: #ffffff;
    color: #3DA9C9;
    padding: 0px 0px 0px 0px;
    vertical-align: middle;
    text-align: center;
    border-collapse: separate;
}

#css_tableau th.week {
    color: #666666;
    background-color: #fafae0;
}

.css_tableau th div {
    width: 25px;
    font-size: 12px;
    background-color: #3DA9C9;
    color: #FFFFFF;
    font-weight: bold;
    padding: 0px 0px 0px 0px;
    vertical-align: middle;
    text-align: center;
    border-collapse: separate;
}

.css_tableau td {
    vertical-align: top;
    font-size: 12px;
    padding: 1px;
    margin: 1px;
    border-spacing: 2px;
    border-color: gray;
    height: 20px;
}

.css_tableau td.week {
    background-color: #ffffff;
    width: 25px;
}

.css_tableau td.weekend {
    background-color: #dddddd;
}

.css_tableau th.today {
    background-color: #F98F7D;
    color: #ffffff;
}

.css_tableau td.today {
    background-color: #FCC4BA;
    color: #ffffff;
}

#layerPersonnes {
    background-color: #ebeff2;
    border-color: #3DA9C9;
    border-width: 1px;
    font-size: 12px;
    border-style: solid;
    width: 100%;
    border-collapse: separate;
    border-spacing: 1px;
}

#layerPersonnes tr {
    background-color: #3DA9C9;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    padding: 0px 0px 0px 0px;
    vertical-align: middle;
}

.css_tableau_user {
    background-color: #ebeff2;
    /*border-color: #3DA9C9;*/
    /*border-width: 1px;*/
    /*border-style: solid;*/
    /*border-collapse: separate;*/
    /*border-spacing: 1px;*/
}

.css_tableau_user tr {
    font-size: 11.5px;
    background-color: #FFFFFF;
    padding: 0px 0px 0px 0px;
    vertical-align: left;
    text-align: left;
    /*border-collapse: separate;*/
}

.css_tableau_user td div {
    font-size: 11.5px;
    background-color: #ffffff;
    color: #000000;
    padding: 0px 0px 0px 0px;
    vertical-align: left;
    text-align: left;
    /*border-collapse: separate;*/
}

</style>
<?php

$annee = 2014;
$mois = 9;
$moiSem = array('JANVIER', 'FERVIER', 'MARS', 'AVRIL', 'MAI', 'JUIN', 'JUILLET', 'AOUT', 'SEPTEMBRE', 'OCTOBRE', 'NOVEMBRE', 'DECEMBRE');
$joursem = array('D', 'L', 'M', 'M', 'J', 'V', 'S');
$user = array('Manantsoa', 'Franckie', 'Mamitiana', 'Lanto', 'Doris', 'Solo', 'Fanja', 'Zo', 'Hantasoa');
$numJour = cal_days_in_month(CAL_GREGORIAN, $mois, $annee);
$nbUser = count($user);

print'<table>';
    print'<tr>';

        print'<td valign="top">';
            print'<table class="css_tableau_user">';
                      print'<tr>';
                          print'<td>&nbsp</td>';
                      print'</tr>';
                      print'<tr>';
                          print'<td>&nbsp</td>';
                      print'</tr>';
                      print'<tr>';
                          print'<td>&nbsp</td>';
                      print'</tr>';
                  for($j=0;$j<$nbUser;$j++){
                      print'<tr>';
                          print'<td><div>'.$user[$j].'</div></td>';
                      print'</tr>';
                  }
              
            print'</table>';
        print'</td>';

        print'<td valign="top">';
            print'<table>';

                print'<tr>';
                print'<td>';
                    print'<table class="css_tableau">';
                          print'<tr>';
                              print'<th colspan='.$numJour.'>'.$moiSem[$mois-1].' '.$annee.'</th>';
                            print'</tr>';
                          print'<tr>';
                              for($i=0;$i<$numJour;$i++){
                                      print'<th><div>'.$joursem[date("w", mktime(0, 0, 0, $mois, ($i+1), $annee))].'</div></th>';
                              }
                          print'</tr>';
                          print'<tr>';
                              for($i=0;$i<$numJour;$i++){
                                      print'<th align="center"><div>'.($i+1).'</div></th>';
                              }
                          print'</tr>';
                
                          for($j=0;$j<$nbUser;$j++){
                              print'<tr>';
                                  for($i=0;$i<$numJour;$i++){
                                      if(date("w", mktime(0, 0, 0, $mois, ($i+1), $annee))==0 || date("w", mktime(0, 0, 0, $mois, ($i+1), $annee))==6){
                                          print'<td class="weekend">&nbsp</td>';
                                      }else{
                                          print'<td>&nbsp</td>';
                                      }
                                          
                                }
                              print'</tr>';
                            }
                    print'</table>';