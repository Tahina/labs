<?php include('functions.php'); ?>
<?php 
$content = getContent('title.txt');
?>
<!DOCTYPE html>
<html lang='fr'>
<head>
	<meta http-equiv="Content-Type" content="" />
	<title>Sitepress</title>
    <!-- general css -->
    <link rel="stylesheet" href="css/style.css" type="text/css"  />

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/functs.js"></script>

    <!-- Fancybox -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" />
    <script src="js/jquery.fancybox.pack.js"></script>
</head>

<body>
    <div id="wrapper" class="full_width_popup">
        <!-- <div id="header">HEADER</div> --> <!-- #header -->
        <div id="content">
            <div id="post" class="left">
                <form name="ajaxform" id="ajaxform" action="ajax-form-submit.php" method="POST">
                    <input type="text" name="value" value="Ceci est un titre" /></br>
                    <input type="hidden" name="field" value="title" />
                    <input type="submit" id="simple-post" value="Enregistrer" />
                </form>
            </div>
            <div class="clear_left"></div>
            <!-- <div id="sidebar" class="left">SIDEBAR</div>
             -->
        </div> <!-- #content -->
        <!-- <div id="footer">FOOTER</div> --> <!-- #footer -->
    </div> <!-- #wrapper -->
    <div id="output2"></div>
</body>
</html>