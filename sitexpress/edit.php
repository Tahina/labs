<?php include('functions.php'); ?>
<?php 
$content = getContent('content.txt');
$title = getContent('title.txt');
?>
<!DOCTYPE html>
<html lang='fr'>
<head>
	<meta http-equiv="Content-Type" content="" />
	<title>Sitepress</title>
    <!-- general css -->
    <link rel="stylesheet" href="css/style.css" type="text/css"  />

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>

    <script src="js/jquery.jeditable.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.generateId.js"></script>
    <script src="js/jquery.jeditable.ckeditor.js"></script>
    
    <!-- Fancybox -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" />
    <script src="js/jquery.fancybox.pack.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.fancybox').fancybox()
            .fancybox({
                    padding : 0,
                    maxWidth    : 630,
                    maxHeight   : 469,                    
                    //fitToView   : true,
                    //width       : '80%',
                    //height      : '80%',
                    autoSize    : true,
                    closeClick  : true,
                    type        : 'iframe'
                });
        });

        jQuery(document).ready(function($) {
             $('.edit').editable('http://127.0.0.1/labs/sitexpress/save.php', {
                 indicator : 'Saving...',
                 tooltip   : 'Cliquer pour modifier'
             });
             $('.edit_area').editable('http://127.0.0.1/labs/sitexpress/save.php', { 
                 loadurl  : 'http://127.0.0.1/labs/sitexpress/load.php?field=content',
                 type      : 'ckeditor',                 
                 onblur    : 'ignore',
                 cancel    : 'Cancel',
                 submit    : 'OK',
                 indicator : '<img src="images/indicator.gif">',
                 tooltip   : 'Cliquer pour modifier'                
             });
         });
    </script>        
</head>

<body>
    <div id="wrapper" class="full_width">
        <div id="header">HEADER</div> <!-- #header -->
        <div id="content">
            <div id="post" class="left">
                
                <!-- <div id="post_title"> -->
                    <!-- <div class="edit"><a class="fancybox fancybox.iframe" href="edit-title.php">Edit</a></div> -->
                    <div class="edit title" id="title"><?php echo $title ?></div>
                <!-- </div> -->

                <div id="post_content">
                    <!-- <div class="edit"><a class="fancybox fancybox.iframe" href="edit-content.php">Edit</a></div> -->
                    <div class="edit_area" id="content"><?php echo $content ?></div>
                </div>

            </div>
            <div id="sidebar" class="left">SIDEBAR</div>
            <div class="clear_left"></div>
        </div> <!-- #content -->
        <div id="footer">FOOTER</div> <!-- #footer -->
    </div> <!-- #wrapper -->
</body>
</html>