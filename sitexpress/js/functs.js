jQuery(document).ready(function($) { 
    var options = { 
        target:        '#output2',   // target element(s) to be updated with server response 
        //beforeSubmit:showLoader  // pre-submit callback 
        success:    fancyBoxClose  // post-submit callback 
 
        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 

    // bind 'myForm' and provide a simple callback function 
    $('#ajaxform').submit(function() { 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(options); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    }); 

    // post-submit callback 
    function showResponse(responseText, statusText, xhr, $form)  { 
        // for normal html responses, the first argument to the success callback 
        // is the XMLHttpRequest object's responseText property 
     
        // if the ajaxSubmit method was passed an Options Object with the dataType 
        // property set to 'xml' then the first argument to the success callback 
        // is the XMLHttpRequest object's responseXML property 
     
        // if the ajaxSubmit method was passed an Options Object with the dataType 
        // property set to 'json' then the first argument to the success callback 
        // is the json data object returned by the server 
     
        alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
            '\n\nThe output div should have already been updated with the responseText.');         
    }

    // post-submit callback 
    function showLoader()  { 
        $('#output2').html('<b>Loading...</b>');
    }

    function fancyBoxClose(){        
        parent.location.reload();
        parent.jQuery.fancybox.close();        
    }

    function CKupdate(){
        for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();
    }

    /* DRAGGABLE */
    $('.draggable').draggable({
        containement : '#content',
        cursor: 'move',
        revert : true
    });

    $('.landing').droppable({
        accept : '#widget div',
        drop: handleDropEvent,
        hoverClass : 'hovered'                
    });

    function handleDropEvent( event, ui ) {
        var draggable = ui.draggable;
        var slotId = $(this).attr( 'data' );
        var draggableId = ui.draggable.attr( 'data' );
        //alert( 'The square with ID "' + draggable.attr('id') + '" was dropped onto me!' );
        
        //alert(draggableId + ' onto ' + slotId);

        $(this).addClass('resetDroppable');
        
        if ( draggableId == slotId ) {
            ui.draggable.addClass( 'correct' );
            ui.draggable.draggable( 'disable' );
            $(this).droppable( 'disable' );
            ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
            ui.draggable.draggable( 'option', 'revert', false );

            // if title
            if(draggableId == 'title') {
                $('#post_header').html('<input type="text" name="title" class="draggable" data="widget" id="content_title" />');
            } else if(draggableId == 'content') { // if content
                $('#post_content').html('<textarea id="editor1" class="draggable" data="widget" name="content">Votre contenu ici...</textarea>');
                CKEDITOR.replace( 'editor1' );
            }
        }
    }
}); 