<?php include('functions.php'); ?>
<?php 
$content = getContent('content.txt');
?>
<!DOCTYPE html>
<html lang='fr'>
<head>
	<meta http-equiv="Content-Type" content="" />
	<title>Sitepress</title>
    <!-- general css -->
    <link rel="stylesheet" href="css/style.css" type="text/css"  />

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/functs.js"></script>
    <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>

    <!-- Fancybox -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" />
    <script src="js/jquery.fancybox.pack.js"></script>

    <script type="text/javascript">
        function CKupdate(){
            for ( instance in CKEDITOR.instances )
                CKEDITOR.instances[instance].updateElement();
        }
    </script>
</head>

<body>
    <div id="wrapper" class="full_width">        
        <div id="content">
            <div id="post" class="left">
                <div id="post_content">
                    <form name="ajaxform" id="ajaxform" action="ajax-form-submit.php" method="POST">
                        <textarea id="editor1" name="content"><?php echo $content; ?></textarea>
                        <input type="hidden" name="field" value="content" />
                        <input type="submit" id="simple-post" value="Enregistrer" onClick="CKupdate();" />
                    </form>
                </div>

            </div>            
            <div class="clear_left"></div>
        </div> <!-- #content -->
        <!-- <div id="footer">FOOTER</div> --> <!-- #footer -->
        <div id="output2"></div>
    </div> <!-- #wrapper -->
    <script type="text/javascript">
        CKEDITOR.replace( 'editor1' );
    </script>
</body>
</html>