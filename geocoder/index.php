<?php
# entrer une adresse à chercher avec google maps
$adresse = isset($_GET['adresse'])?$_GET['adresse']:'google';

# démarrer google maps et afficher le tableau json contenant les données de géolocalisation de l'adresse
$url = sprintf("http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false", htmlentities(urlencode($adresse)));
echo (file_get_contents($url));
?>