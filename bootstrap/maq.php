<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="css/style.css" rel="stylesheet">  
	<link href="css/jumbotron-narrow.css" rel="stylesheet">
	<link href="css/carousel.css" rel="stylesheet">
	<link href="css/sticky-footer.css" rel="stylesheet">

  <style type="text/css">
  	<!--  		
      @media only screen and (min-width : 768px) {
          /* Make Navigation Toggle on Desktop Hover */
          .dropdown:hover .dropdown-menu {
              display: block;
          }
      }

      .grid-gal { padding-bottom: 4px; }
      /*.grid-gal img { width: 100%; max-width: 100%; }*/

      .small-gutter > [class*='col-'] {
          padding-right:2px;
          padding-left:2px;
      }

      .btn-yellow { background-color: yellow; border-color: #333; color: #333; }
  	-->
  </style>

  
</head>
<body>

<div class="container">
	<div class="row" style="border-top:1px solid red;">
		<!-- <div class="col-md-4">
			LOGO
		</div> --> <!-- .sidebar -->
    <nav class="navbar" role="navigation">
      <div class="container-fluid">
    			<!-- nav -->
          <!-- navbar header section -->
          <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">Kicks!</a>
          </div>
          <!-- navbar menu section -->
          <div class="collapse navbar-collapse navbar-responsive-collapse">
               <ul class="nav navbar-nav">
               <li><a href="#">Home</a></li>
               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Package<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                         <li><a href="#">Malaysia-Thailand</a></li>
                         <li><a href="#">Singapore</a></li>
                    </ul>
               </li>
               <li><a href="#">Support</a></li>
               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tickets<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                         <li><a href="#">Intl</a></li>
                         <li><a href="#">Regional</a></li>
                    </ul>
               </li>
               </ul>

               <!-- search form -->
               <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search">
               </form>
          
        </div>
      </div>    
      <!-- .nav -->
		</div> <!-- .content -->
	</div> <!-- .row -->
</div> <!-- .container -->

<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <!-- <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide"> -->
          <img class="first-slide" src="images/curry.jpg" alt="First slide">

          <div class="container">
            <div class="carousel-caption">
              <h1>Curry One Black & Gold Banner</h1>
              <p>Charged with belief.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Order now</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="images/curry1.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Curry One Scratch Green</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-danger" href="#" role="button">Shop</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="images/curry2.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Curry One Scratch White</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-danger btn-yellow" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

<div class="container">
	<div class="row">
		<div class="col-md-4">
			SIDEBAR
		</div> <!-- .sidebar -->
		<div class="col-md-8">
			 <!-- gallery -->
       <div class="container">
        <section class="row small-gutter">
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
          <div class="col-xs-4 col-sm-3 col-md-2 grid-gal"><img src="images/t1.jpg" class="img-responsive" alt="Tigre"></div>
        </section>
      </div>
       <!-- .gallery -->
		</div> <!-- .content -->
	</div> <!-- .row -->
</div> <!-- .container -->

<!-- <footer class="footer">
  <div class="container">
    <p class="text-muted text-center">&copy; owner 2015</p>
  </div>
</footer> --> <!-- .footer -->

</body>
</html>