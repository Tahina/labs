<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <style type="text/css">
  	<!--  		
		
  	-->
  </style>

  
</head>
<body>

<div class="container">
	<div class="page-header">
		<h1>Example Page Header</h1>
		
		<nav class="navbar navbar-inverse" role="navigation">
		    <div class="container-fluid">
		          <!-- navbar header section -->
		          <div class="navbar-header">
		               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
		               <span class="sr-only">Toggle navigation</span>
		               <span class="icon-bar"></span>
		               <span class="icon-bar"></span>
		               <span class="icon-bar"></span>
		               </button>
		               <a class="navbar-brand" href="#">Way2Travel.com</a>
		          </div>
		          <!-- navbar menu section -->
		          <div class="collapse navbar-collapse navbar-responsive-collapse">
		               <ul class="nav navbar-nav">
		               <li><a href="#">Home</a></li>
		               <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Package<b class="caret"></b></a>
		                    <ul class="dropdown-menu">
		                         <li><a href="#">Malaysia-Thailand</a></li>
		                         <li><a href="#">Singapore</a></li>
		                    </ul>
		               </li>
		               <li><a href="#">Support</a></li>
		               <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tickets<b class="caret"></b></a>
		                    <ul class="dropdown-menu">
		                         <li><a href="#">Intl</a></li>
		                         <li><a href="#">Regional</a></li>
		                    </ul>
		               </li>
		               </ul>

		               <!-- search form -->
		               <form class="navbar-form navbar-right">
		                    <input type="text" class="form-control" placeholder="Search">
		               </form>
		          </div>
		     </div>
		</nav>

		<!-- <div class="dropdown">
			<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Tutorials
			<span class="caret"></span></button>
			<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
				<li role="presentation"><a role="menuitem" href="#">HTML</a></li>
				<li role="presentation"><a role="menuitem" href="#">CSS</a></li>
				<li role="presentation"><a role="menuitem" href="#">JavaScript</a></li>
				<li role="presentation" class="divider"></li>
				<li role="presentation"><a role="menuitem" href="#">About Us</a></li>
			</ul>
		</div> -->
	</div>	<!-- .page-header -->
</div>

</body>
</html>